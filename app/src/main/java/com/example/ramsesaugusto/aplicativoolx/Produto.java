package com.example.ramsesaugusto.aplicativoolx;

import java.util.Calendar;
import java.util.Date;

/**
 * Created by ramsesaugusto on 23/04/2018.
 */

public class Produto {
    private int id;
    private String descricao;
    private double preco;
    private Date dataPostagem;
    private String localizacao;
    private String urlFoto1;
    private String urlFoto2;
    private String urlFoto3;
    private String urlFoto4;

    public Produto(int id, String descricao, double preco, Date dataPostagem, String localizacao) {
        this.descricao = descricao;
        this.preco = preco;
        this.dataPostagem = dataPostagem;
        this.localizacao = localizacao;
        this.id = id;
    }

    public Produto(int id, String descricao, double preco, Date dataPostagem, String localizacao, String urlFoto1) {
        this.descricao = descricao;
        this.preco = preco;
        this.dataPostagem = dataPostagem;
        this.localizacao = localizacao;
        this.urlFoto1 = urlFoto1;
        this.id = id;
    }

    public Produto(int id, String descricao, double preco, Date dataPostagem, String localizacao, String urlFoto1, String urlFoto2, String urlFoto3) {
        this.descricao = descricao;
        this.preco = preco;
        this.dataPostagem = dataPostagem;
        this.localizacao = localizacao;
        this.urlFoto1 = urlFoto1;
        this.urlFoto2 = urlFoto2;
        this.urlFoto3 = urlFoto3;
        this.id = id;
    }

    public Produto(int id, String descricao, double preco, Date dataPostagem, String localizacao, String urlFoto1, String urlFoto2, String urlFoto3, String urlFoto4) {
        this.descricao = descricao;
        this.preco = preco;
        this.dataPostagem = dataPostagem;
        this.localizacao = localizacao;
        this.urlFoto1 = urlFoto1;
        this.urlFoto2 = urlFoto2;
        this.urlFoto3 = urlFoto3;
        this.urlFoto4 = urlFoto4;
        this.id = id;
    }

    public int getQtdeFotos(){
        int totalFotos = 0;
        if(this.urlFoto1 != null){
            totalFotos++;
        }
        if(this.urlFoto2 != null){
            totalFotos++;
        }
        if(this.urlFoto3 != null){
            totalFotos++;
        }
        if(this.urlFoto4 != null){
            totalFotos++;
        }
        return totalFotos;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public Date getDataPostagem() {
        return dataPostagem;
    }

    public void setDataPostagem(Date dataPostagem) {
        this.dataPostagem = dataPostagem;
    }

    public String getLocalizacao() {
        return localizacao;
    }

    public void setLocalizacao(String localizacao) {
        this.localizacao = localizacao;
    }

    public String getUrlFoto1() {
        return urlFoto1;
    }

    public void setUrlFoto1(String urlFoto1) {
        this.urlFoto1 = urlFoto1;
    }

    public String getUrlFoto2() {
        return urlFoto2;
    }

    public void setUrlFoto2(String urlFoto2) {
        this.urlFoto2 = urlFoto2;
    }

    public String getUrlFoto3() {
        return urlFoto3;
    }

    public void setUrlFoto3(String urlFoto3) {
        this.urlFoto3 = urlFoto3;
    }

    public String getUrlFoto4() {
        return urlFoto4;
    }

    public void setUrlFoto4(String urlFoto4) {
        this.urlFoto4 = urlFoto4;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
