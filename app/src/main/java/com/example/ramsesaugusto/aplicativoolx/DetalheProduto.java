package com.example.ramsesaugusto.aplicativoolx;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ViewFlipper;

import java.util.ArrayList;
import java.util.List;

public class DetalheProduto extends Activity  {
    private static Produto produto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe_produto);



        int idProduto = (int) getIntent().getLongExtra("idProduto", -1);
        //Produto produto = prod.get(idProduto);

        /*ArrayList<Produto> produtoList = ControllerProduto.getInstance().getProdutoList();
        this.produto = produtoList.get(idProduto);*/

        this.produto = ControllerProduto.getInstance().getProduto(idProduto);


        //PASSAR ARGUMENTO PARA O FRAGMENT
        /*Bundle args = new Bundle();
        args.putInt("idProduto", idProduto);
        DetalheProdutoImage detalheProdutoImage = new DetalheProdutoImage();
        detalheProdutoImage.setArguments(args);*/

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayoutDetalheProdutoImagem, DetalheProdutoImage.newInstance());
        transaction.replace(R.id.frameLayoutDetalheProdutoDescricao, DetalheProdutoDescricao.newInstance());
        transaction.commit();

    }


    public static Produto getProduto() {
        return produto;
    }

}
