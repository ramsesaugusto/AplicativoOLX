package com.example.ramsesaugusto.aplicativoolx;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;


public class ItemOneFragment extends Fragment implements AdapterView.OnItemClickListener {
    private EditText edtxtPesquisa;
    private ProdutoAdapter produtoAdapter;

    public static ItemOneFragment newInstance() {
        ItemOneFragment fragment = new ItemOneFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View itemFragmentView = inflater.inflate(R.layout.fragment_item_one, container, false);

        edtxtPesquisa = itemFragmentView.findViewById(R.id.edtxtPesquisa);
        edtxtPesquisa.addTextChangedListener(new TextFilter());



        produtoAdapter = new ProdutoAdapter(getActivity(), ControllerProduto.getInstance().getProdutoList());

        ListView lViewProdutos;
        lViewProdutos = itemFragmentView.findViewById(R.id.lViewProdutos);
        lViewProdutos.setAdapter(produtoAdapter);

        lViewProdutos.setOnItemClickListener(this);

        return itemFragmentView;
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        Intent intent = new Intent(getActivity(), DetalheProduto.class);
        intent.putExtra("idProduto", l);
        startActivity(intent);
    }


    public class TextFilter implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            //Somente procurar quando a pessoa digita o enter na busca
            /*if ((charSequence.length() > 0) && (charSequence.subSequence(charSequence.length() - 1, charSequence.length()).toString().equalsIgnoreCase("\n"))) {
                produtoAdapter.getFilter().filter(charSequence);
            }*/
            produtoAdapter.getFilter().filter(charSequence);

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }


    }
}
