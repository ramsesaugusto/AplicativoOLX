package com.example.ramsesaugusto.aplicativoolx;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.InputStream;

/**
 * Created by ramsesaugusto on 09/05/2018.
 */

public class DownloadFoto extends AsyncTask<String, Void, Bitmap> {


    private ImageView imgViewProduto;

    public DownloadFoto(ImageView imageView) {
        this.imgViewProduto = imageView;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        this.imgViewProduto.setImageBitmap(result);
        //bmImage.setImageBitmap(result);
    }
}
