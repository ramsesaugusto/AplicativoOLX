package com.example.ramsesaugusto.aplicativoolx;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * Created by ramsesaugusto on 09/05/2018.
 */

public class ControllerProduto {
    private static ControllerProduto instance;
    private ArrayList<Produto> produtoList;

    public ControllerProduto(){
        produtoList = new ArrayList<Produto>();
        produtoList.add(new Produto(1, "Macbook pro 15", 2000, new GregorianCalendar(2018, 11, 21).getTime(), "Taguatinga",
                "https://assets.pcmag.com/media/images/366795-apple-macbook-pro-15-inch-retina-display-2014.jpg?width=1000&height=563",
                "https://hothardware.com/articleimages/Item1538/small_MacBook-Pro-overview-shot-from-apple.jpg",
                "https://hothardware.com/articleimages/Item1538/small_MacBook-Pro-side-angle-closed-view.jpg"));
        produtoList.add(new Produto(2, "Iphone X", 6000, new GregorianCalendar(2018, 7, 12).getTime(), "Plano piloto",
                "https://store.storeimages.cdn-apple.com/4975/as-images.apple.com/is/image/AppleInc/aos/published/images/i/ph/iphone/x/iphone-x-silver-select-2017?wid=305&hei=358&fmt=jpeg&qlt=95&op_usm=0.5,0.5&.v=1515602510472"));
        produtoList.add(new Produto(3, "Ipad 2", 1500, new GregorianCalendar(2018, 3, 16).getTime(), "Aguas Claras",
                "https://images.techhive.com/images/article/2014/01/switchtoipadmini2_primary-100225274-large.jpg",
                "https://zdnet4.cbsistatic.com/hub/i/r/2016/04/05/ea0f8995-9ea8-4183-bd05-cf0caa0e5631/thumbnail/770x433/8e6c645102836dbbe337990ed68edd3c/ipad-pro-2016.jpg",
                "http://images.lojaiplace.com.br/imagens/small/ipad-apple-space-gray_z_small.jpg?t=",
                "https://evolvingitsm.files.wordpress.com/2015/01/ipad_1_wifi_small.png?w=525"));
    }

    public Produto getProduto(int id){
        for(Produto produto : produtoList){
            if(produto.getId() == id){
                return produto;
            }
        }
        return null;
    }

    public static ControllerProduto getInstance(){
        if(instance == null){
            instance = new ControllerProduto();

        }
        return instance;
    }

    public ArrayList<Produto> getProdutoList() {
        return produtoList;
    }
}
