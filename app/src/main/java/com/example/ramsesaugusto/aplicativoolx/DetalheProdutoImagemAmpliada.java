package com.example.ramsesaugusto.aplicativoolx;


import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ViewFlipper;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link DetalheProdutoImagemAmpliada#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DetalheProdutoImagemAmpliada extends Fragment implements View.OnTouchListener, View.OnClickListener{
    private double inicialX;
    private ViewFlipper viewFlipper;
    private Button btnBackImagemAmpliada;
    private Produto produto;
    private ImageView imgViewFotoProdutoAmpliada1;
    private ImageView imgViewFotoProdutoAmpliada2;
    private ImageView imgViewFotoProdutoAmpliada3;
    private ImageView imgViewFotoProdutoAmpliada4;



    public static DetalheProdutoImagemAmpliada newInstance() {
        DetalheProdutoImagemAmpliada fragment = new DetalheProdutoImagemAmpliada();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detalhe_produto_imagem_ampliada, container, false);

        viewFlipper = view.findViewById(R.id.viewFlipperProdutoFotoAmpliada);
        viewFlipper.setOnTouchListener(this);

        viewFlipper.setDisplayedChild(this.getArguments().getInt("idImage"));

        this.produto = DetalheProduto.getProduto();

        imgViewFotoProdutoAmpliada1 = view.findViewById(R.id.imgViewProdutoFotoAmpliada1);
        imgViewFotoProdutoAmpliada2 = view.findViewById(R.id.imgViewProdutoFotoAmpliada2);
        imgViewFotoProdutoAmpliada3 = view.findViewById(R.id.imgViewProdutoFotoAmpliada3);
        imgViewFotoProdutoAmpliada4 = view.findViewById(R.id.imgViewProdutoFotoAmpliada4);

        DownloadFoto downloadFoto1 = new DownloadFoto(imgViewFotoProdutoAmpliada1);
        downloadFoto1.execute(this.produto.getUrlFoto1());

        DownloadFoto downloadFoto2 = new DownloadFoto(imgViewFotoProdutoAmpliada2);
        downloadFoto2.execute(this.produto.getUrlFoto2());

        DownloadFoto downloadFoto3 = new DownloadFoto(imgViewFotoProdutoAmpliada3);
        downloadFoto3.execute(this.produto.getUrlFoto3());

        DownloadFoto downloadFoto4 = new DownloadFoto(imgViewFotoProdutoAmpliada4);
        downloadFoto4.execute(this.produto.getUrlFoto4());


        btnBackImagemAmpliada = view.findViewById(R.id.btnBackImagemAmpliada);
        btnBackImagemAmpliada.setOnClickListener(this);

        return view;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                inicialX = motionEvent.getX();
                return true;
            case MotionEvent.ACTION_UP:
                double finalX = motionEvent.getX();
                if(inicialX > finalX){
                    if(viewFlipper.getDisplayedChild() == (this.produto.getQtdeFotos() - 1)){
                        break;
                    }
                    viewFlipper.showNext();
                }else{
                    if(viewFlipper.getDisplayedChild() == 0){
                        break;
                    }
                    viewFlipper.showPrevious();
                }
                break;
        }
        return false;
    }

    @Override
    public void onClick(View view) {
        if(view.getId() == R.id.btnBackImagemAmpliada){
            FragmentTransaction transaction = getFragmentManager().beginTransaction();
            transaction.replace(R.id.frameLayoutImagemAmpliada, new Fragment());
            transaction.commit();

        }
    }
}
