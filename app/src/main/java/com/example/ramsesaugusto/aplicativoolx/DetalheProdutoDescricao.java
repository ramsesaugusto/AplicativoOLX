package com.example.ramsesaugusto.aplicativoolx;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetalheProdutoDescricao extends Fragment implements View.OnClickListener{
    private Button btnDetalheProdutoVerTelefone;
    private TextView txtViewDetalheProdutoNome;
    private TextView txtViewDetalheProdutoPreco;
    private TextView txtViewDetalheProdutoDataHora;
    Produto produto;

    public static DetalheProdutoDescricao newInstance() {
        DetalheProdutoDescricao fragment = new DetalheProdutoDescricao();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detalhe_produto_descricao, container, false);

        produto = DetalheProduto.getProduto();

        btnDetalheProdutoVerTelefone = view.findViewById(R.id.btnDetalheProdutoVerTelefone);
        btnDetalheProdutoVerTelefone.setOnClickListener(this);

        txtViewDetalheProdutoNome = view.findViewById(R.id.txtViewDetalheProdutoNome);
        txtViewDetalheProdutoNome.setText(produto.getDescricao());

        txtViewDetalheProdutoPreco = view.findViewById(R.id.txtViewDetalheProdutoPreco);
        txtViewDetalheProdutoPreco.setText(NumberFormat.getCurrencyInstance(view.getResources().getConfiguration().locale).format(produto.getPreco()));

        txtViewDetalheProdutoDataHora = view.findViewById(R.id.txtViewDetalheProdutoDataHora);
        txtViewDetalheProdutoDataHora.setText(new SimpleDateFormat("dd/MM/yyyy").format(produto.getDataPostagem()));

        return view;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDetalheProdutoVerTelefone:
                Uri call = Uri.parse("tel: 999999999");
                Intent newIntent = new Intent(Intent.ACTION_DIAL, call);

                startActivity(newIntent);
                break;
        }
    }

}
