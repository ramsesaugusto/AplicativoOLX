package com.example.ramsesaugusto.aplicativoolx;


import android.app.FragmentTransaction;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ViewFlipper;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetalheProdutoImage extends Fragment implements View.OnTouchListener {
    private ViewFlipper viewFlipperProdutos;
    private double initialX;
    private ImageView imgViewFotoProduto1;
    private ImageView imgViewFotoProduto2;
    private ImageView imgViewFotoProduto3;
    private ImageView imgViewFotoProduto4;
    private Produto produto;


    public static DetalheProdutoImage newInstance() {
        DetalheProdutoImage fragment = new DetalheProdutoImage();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_detalhe_produto_image, container, false);

        viewFlipperProdutos = view.findViewById(R.id.viewFlipperProdutos);
        viewFlipperProdutos.setOnTouchListener(this);

        //int idProduto = this.getArguments().getInt("idProduto");
        //Produto produto = ControllerProduto.getInstance().getProdutoList().get(idProduto);

        this.produto = DetalheProduto.getProduto();

        imgViewFotoProduto1 = view.findViewById(R.id.imgViewProduto1);
        imgViewFotoProduto2 = view.findViewById(R.id.imgViewProduto2);
        imgViewFotoProduto3 = view.findViewById(R.id.imgViewProduto3);
        imgViewFotoProduto4 = view.findViewById(R.id.imgViewProduto4);

        DownloadFoto downloadFoto1 = new DownloadFoto(imgViewFotoProduto1);
        downloadFoto1.execute(this.produto.getUrlFoto1());

        DownloadFoto downloadFoto2 = new DownloadFoto(imgViewFotoProduto2);
        downloadFoto2.execute(this.produto.getUrlFoto2());

        DownloadFoto downloadFoto3 = new DownloadFoto(imgViewFotoProduto3);
        downloadFoto3.execute(this.produto.getUrlFoto3());

        DownloadFoto downloadFoto4 = new DownloadFoto(imgViewFotoProduto4);
        downloadFoto4.execute(this.produto.getUrlFoto4());


        return view;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = motionEvent.getX();
                return true;
            case MotionEvent.ACTION_UP:
                float finalX = motionEvent.getX();
                if(initialX == finalX){
                    DetalheProdutoImagemAmpliada detalheProdutoImagemAmpliada = new DetalheProdutoImagemAmpliada();
                    Bundle idImage = new Bundle();
                    idImage.putInt("idImage", viewFlipperProdutos.getDisplayedChild());
                    detalheProdutoImagemAmpliada.setArguments(idImage);


                    FragmentTransaction transaction = getFragmentManager().beginTransaction();
                    transaction.replace(R.id.frameLayoutImagemAmpliada, detalheProdutoImagemAmpliada);
                    transaction.commit();
                }else{
                    if (initialX > finalX) {
                        if (viewFlipperProdutos.getDisplayedChild() == (this.produto.getQtdeFotos() - 1)) {
                            break;
                        }
                        viewFlipperProdutos.showNext();
                    } else {
                        if (viewFlipperProdutos.getDisplayedChild() == 0) {
                            break;
                        }
                        viewFlipperProdutos.showPrevious();
                    }
                }
                break;
        }
        return false;
    }
}
