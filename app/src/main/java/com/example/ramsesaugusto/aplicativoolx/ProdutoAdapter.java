package com.example.ramsesaugusto.aplicativoolx;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ramsesaugusto on 24/04/2018.
 */

public class ProdutoAdapter extends BaseAdapter implements Filterable {
    private Produto produto;
    private Context context;
    private List<Produto> produtoList;
    private List<Produto> produtoListFitered;


    public ProdutoAdapter(Context context, List<Produto> produtoList) {
        this.context = context;
        this.produtoList = produtoList;
        produtoListFitered = produtoList;
    }

    @Override
    public int getCount() {
        return produtoListFitered.size();
    }

    @Override
    public Object getItem(int i) {
        return produtoListFitered.get(i);
    }

    @Override
    public long getItemId(int i) {
        return produtoListFitered.get(i).getId();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //produto = (Produto) getItem(i);
        if(produtoListFitered.size() == 0){
            return null;
        }else{
            produto = produtoListFitered.get(i);

            View linha = LayoutInflater.from(context).inflate(R.layout.item_produtos, null);


            ImageView imgViewProduto = linha.findViewById(R.id.imgProduto);
            TextView txtViewDescricao = linha.findViewById(R.id.txtViewDescricao);
            TextView txtViewPreco = linha.findViewById(R.id.txtViewPreco);
            TextView txtViewDataLocalizacao = linha.findViewById(R.id.txtViewDataLocalizacao);

        /*Resources resources = context.getResources();
        TypedArray typedArray = resources.obtainTypedArray(R.array.produtos);
        imgViewProduto.setImageDrawable(typedArray.getDrawable(i));*/

            DownloadFoto downloadImageTask = new DownloadFoto(imgViewProduto);
            downloadImageTask.execute(produto.getUrlFoto1());

            txtViewDescricao.setText(produto.getDescricao());
            txtViewPreco.setText(NumberFormat.getCurrencyInstance(context.getResources().getConfiguration().locale).format(produto.getPreco()));
            txtViewDataLocalizacao.setText(produto.getLocalizacao() + " - " + new SimpleDateFormat("dd/MM/yyyy").format(produto.getDataPostagem()));

            /*linha.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(view.getContext(), DetalheProduto.class);
                    intent.putExtra("idProduto", produto.getId());
                    view.getContext().startActivity(intent);
                }
            });*/

            return linha;
        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                FilterResults results = new FilterResults();
                int i = charSequence.length();
                //If there's nothing to filter on, return the original data for your list
                if (charSequence.length() == 0) {
                    results.values = produtoList;
                    results.count = produtoList.size();
                } else {
                    ArrayList<Produto> filterResultsData = new ArrayList<Produto>();

                    /*String patternStr = "(?i)";
                    patternStr = patternStr + charSequence.toString().replace("\n", "");
                    patternStr.replace(" ", "\\s+");
                    patternStr = patternStr + "\\s+";*/

                    String patternStr = "(?i)^" + charSequence.toString().replace("\n", "");


                    Pattern pattern = Pattern.compile(patternStr);

                    for (Produto data : produtoList) {
                        Matcher matcher = pattern.matcher(data.getDescricao());
                        if (matcher.find()) {
                            filterResultsData.add(data);
                        }
                    }

                    results.values = filterResultsData;
                    results.count = filterResultsData.size();
                }

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                produtoListFitered = (ArrayList<Produto>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
